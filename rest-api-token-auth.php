<?php

/**
 * Plugin Name: WordPress REST API Token Authentication
 * Plugin URI: https://gitlab.com/rockschtar/wordpress-rest-api-token-auth
 * Description: API-Token based authentication for the WordPress REST-API
 * Version: develop
 * Author: Stefan Helmer
 * Author URI: https://gitlab.com/rockschtar/
 * License: MIT License
 */

define('RSWPRATA_PLUGIN_DIR', plugin_dir_path(__FILE__));

if (file_exists(RSWPRATA_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
    require_once 'vendor/autoload.php';
}

use Rockschtar\WordPress\RESTApiTokenAuth\TokenAuthentication;

TokenAuthentication::init();