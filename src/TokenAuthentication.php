<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\RESTApiTokenAuth;

class TokenAuthentication {

    protected static $_instance;

    protected function __construct() {
        add_filter('rest_request_before_callbacks', array(&$this, 'check_api_token'), 10, 3);
    }

    public static function init() {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    final public function check_api_token($response, $handler, \WP_REST_Request $request) {
        $attributes = $request->get_attributes();

        if (isset($attributes['rswp_require_api_token']) && $attributes['rswp_require_api_token'] === true) {

            $api_token = $request->get_header('x_api_token');

            if (defined('RSWP_REST_API_TOKEN')) {
                $default_rest_api_token = RSWP_REST_API_TOKEN;
            } else {
                $default_rest_api_token = '';
            }

            $the_token = apply_filters('rswp_rest_api_token', $default_rest_api_token, $handler, $request);

            if ($api_token !== $the_token) {

                $error = new \WP_Error(401, 'Invalid API-Token');
                return apply_filters('rswp_rest_api_invalid_token_error', $error);
            }
        }

        return $response;
    }

    protected function __clone() {

    }

}